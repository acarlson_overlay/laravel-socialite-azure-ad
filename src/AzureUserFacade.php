<?php

namespace Acarlson_Overlay\LaravelSocialiteAzureAd;

use Illuminate\Support\Facades\Facade;

class AzureUserFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'azure-user';
    }
}
